import syslog
from jpype import isJVMStarted, startJVM, getDefaultJVMPath, JClass

"""
    This is the class that interacts with the Hive database and directly with Sentry.
    Anything that needs to access Hive or Sentry, such as permission checking or updating will go through here
"""
class HiveClient():
	def __init__(self, config):
		self.permissionsMap = {
			"all": "*",
			"select": "select"
		}
		self.config = config
		syslog.openlog(self.config.LOG_NAME + "(Hive)")
		self.execute = self.config.EXECUTE
		# Load JARs
		if not isJVMStarted():
			startJVM(getDefaultJVMPath(), "-ea", "-Djava.class.path=%s" % self.config.CLASSPATH)
		self.hive = JClass("Enforcers.HiveEnforcer")(self.config.HIVE_CONN_STR)
		self.currentPermissions = self.hive.getPermissions()

	
	def executeQuery(self, qs):
		return self.hive.executeQuery(qs)
	

	def grantPermissions(self, role, permissions, database, table, column=None):
		tableString = '%s.%s' % (database, table)
		# If permission already granted
		if tableString in self.currentPermissions:
			if self.permissionsMap[permissions] in self.currentPermissions[tableString]:
				if role in self.currentPermissions[tableString][self.permissionsMap[permissions]]:
					statement = "Correct: %s on table %s.%s for role `%s`" % (permissions, database, table, role)
					print statement
					return
		# If column level permission required
		if column:
			statement = "GRANT %s(%s) ON TABLE %s.%s TO ROLE `%s`" % (permissions, column, database, table, role)
			print statement
			if self.execute:
				print "EXECUTED"
				syslog.syslog(statement)
				self.hive.applyPermissions(database, table, column, role, permissions)
			else:
				with open(self.config.REVIEW_FILE, 'a+') as forReview:
					forReview.write(statement)	
			return
		# Other permissions required are table level		
		statement = "GRANT %s ON TABLE %s.%s TO ROLE `%s`" % (permissions, database, table, role)
		print statement
		if self.execute:
			print "EXECUTED"
			syslog.syslog(statement)
			self.hive.applyPermissions(database, table, role, permissions)
		else:
			with open(self.config.REVIEW_FILE, 'a+') as forReview:
				forReview.write(statement)	
		return
		
	

	def revokePermissions(self, allowedRoles, permissions, database, table, column=None):
		tableString = '%s.%s' % (database, table)
		if tableString in self.currentPermissions:
			if self.permissionsMap[permissions] in self.currentPermissions[tableString]:
				for role in self.currentPermissions[tableString][self.permissionsMap[permissions]]:
					if role not in allowedRoles:
						if not column:
							statement = "REVOKE %s ON TABLE %s.%s FROM ROLE `%s`" % (permissions, database, table, role)
							print statement
							if self.execute:
								print "EXECUTED"
								syslog.syslog(statement)
								self.hive.revokePermissions(database, table, role, permissions)
							else:
								with open(self.config.REVIEW_FILE, 'a+') as forReview:
									forReview.write(statement)	
							return
						statement = "REVOKE %s(%s) ON TABLE %s.%s FROM ROLE `%s`" % (permissions, column, database, table, role)
						print statement
						if self.execute:
							print "EXECUTED"
							syslog.syslog(statement)
							self.hive.revokePermissions(database, table, column, role, permissions)
						else:
							with open(self.config.REVIEW_FILE, 'a+') as forReview:
								forReview.write(statement)	
		return
