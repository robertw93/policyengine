import requests, json
from pyhive import hive
from requests_gssapi import HTTPSPNEGOAuth, OPTIONAL


hive_uri = { # URI details for accessing directly in Hive
		'host'					: 'eypoc-mn0.pocinet.local',
		'database'				: 'default',
		'port'					: 10000,
		'auth'					: 'KERBEROS',
		'kerberos_service_name'	: 'hive'
	}
connection_hive = hive.Connection(**hive_uri)
hive_cursor = connection_hive.cursor()

gssapi_auth = HTTPSPNEGOAuth(mutual_authentication=OPTIONAL)
baseURI = "http://eypoc-mn0.pocinet.local:50070/webhdfs/v1"

policy_groups = { # TODO: fill out this with mapping of found groups to correct accounts
	'hive': { "all": [], "select": ['hive'] }
}

query = "SHOW DATABASES"
hive_cursor.execute(query)
groups = {}
for db in hive_cursor.fetchall():
	db = str(db[0])
	query = "SHOW TABLES IN {}".format(db)
	hive_cursor.execute(query)
	for table in hive_cursor.fetchall():
		table = "{}.{}".format(db, str(table[0]))
		query = "DESCRIBE FORMATTED {}".format(table)
		hive_cursor.execute(query)
		for desc in hive_cursor.fetchall():
			col_name = str(desc[0])
			if col_name.strip() == "Location:":
				location = str(desc[1])[35:] # needs to be changed based on server name
				r = requests.get("{}/{}?op=LISTSTATUS".format(baseURI, location), auth=gssapi_auth)
				if str(r.status_code) == "200":
					j = json.loads(r.text)
					try:
						group = str(j["FileStatuses"]["FileStatus"][0]["group"])
					except IndexError:
						group = "i dont exist"
					if group in policy_groups:
						groups[table] = policy_groups[group]
					else:
						groups[table] = { "all": [], "select": ['dft_dft_gu'] } # TODO: put default service groups here
					
# Write JSON file
with open('./../hiveClassifications.json', 'w+') as outfile:
    outfile.write(json.dumps(groups,
                      indent=4,
					  sort_keys=True,
                      separators=(',', ': '),
					  ensure_ascii=False))
	