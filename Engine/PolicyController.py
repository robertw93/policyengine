import syslog, psycopg2
from psycopg2.extensions import AsIs
"""
    This is the class that interacts with the policy engine database.
    Anything that needs to access our the classification labels or the mapping of ad groups and roles to the labels will come through here
"""


class PolicyClient():
	def __init__(self, config):
		self.config = config
		syslog.openlog(self.config.LOG_NAME + "(Policy Metadata)")
		self.connection_policy = psycopg2.connect(self.config.METADATA_URI)
		self.policy_cursor = self.connection_policy.cursor()


	def getRoles(self, group_id):
		"""Return all roles we have stored"""
		where = "WHERE group_list ~ '(^|\\|){}(\\||$)'".format(group_id)
		self.policy_cursor.execute("""
			SELECT name
			FROM ROLE_GROUP_MAPPING
			%s
		""", (AsIs(where),))
		
		return list(sum(self.policy_cursor.fetchall(), ()))


	def getTypedLabel(self, labeltype, value):
		"""Return a label id given a type + value label pair"""
		self.policy_cursor.execute("""
			SELECT id
			FROM Labels
			WHERE type = '%s' AND value = '%s'
		""", (AsIs(labeltype), AsIs(value)))
		label = self.policy_cursor.fetchone()
		if label:
			return label[0]

	def getLabelList(self, bu, domain, confidentiality):
		"""Get a list structure containing all ids for the bu + domain + confidentiality combination provided"""
		label_list = []
		for area in domain:
			fine_grain = self.getTypedLabel("Grain", bu + "_" + area)
			if fine_grain != None:
				label_list.append(fine_grain)
		if len(label_list) < 1:
			label_list.append(self.getTypedLabel("Grain", bu + "_default"))
		label_list.append(self.getTypedLabel("Classification", confidentiality))
		return label_list

	def getAllGroupsWithLabels(self, labels):
		"""Return a set of group ids where the group has all labels in the list provided"""
		for index, label in enumerate(labels):
			if index == 0:
				if label == 0:
					where = "WHERE name = 'dft_dft_gu'"
					break
				where = "WHERE label_list LIKE E'{}|%'".format(label)
			else:
				if index == (len(labels) - 1):
					where += " AND label_list LIKE E'%|{}'".format(label)
				else:
					where += " AND label_list LIKE E'%|{}|%'".format(label)

		self.policy_cursor.execute("""
			SELECT id, label_list
			FROM GROUP_LABEL_MAPPING
			%s
		""", (AsIs(where),))
		res = self.policy_cursor.fetchall()
		return res

	
	def getGroupNames(self, labels):
		"""Return a set of group ids where the group has all labels in the list provided"""
		for index, label in enumerate(labels):
			if index == 0:
				if label == 0:
					where = "WHERE name = 'dft_dft_gu'"
					break
				where = "WHERE label_list LIKE E'{}|%'".format(label)
			else:
				if index == (len(labels) - 1):
					where += " AND label_list LIKE E'%|{}'".format(label)
				else:
					where += " AND label_list LIKE E'%|{}|%'".format(label)

		self.policy_cursor.execute("""
			SELECT name
			FROM GROUP_LABEL_MAPPING
			%s
		""", (AsIs(where),))
		res = self.policy_cursor.fetchall()
		return res[0][0]