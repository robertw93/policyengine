import psycopg2, syslog
from pyhive import hive
from psycopg2.extensions import AsIs
from jpype import isJVMStarted, startJVM, getDefaultJVMPath, JClass

"""
    This is the class that interacts with the Hive database and directly with Sentry.
    Anything that needs to access Hive or Sentry, such as permission checking or updating will go through here
"""
class HiveClient():
	def __init__(self, config):
		self.config = config
		# Initialise the class, connecting to Hive and directly to Sentry
		syslog.openlog(self.config.LOG_NAME + "(Hive)")

		self.execute = self.config.EXECUTE

		# Load JARs
		if not isJVMStarted():
			startJVM(getDefaultJVMPath(), "-ea", "-Djava.class.path=%s" % self.config.CLASSPATH)
		self.hive = JClass("Enforcers.HiveEnforcer")()
		self.currentPermissions = self.hive.getPermissions()

	
	def executeQuery(self, qs):
		return self.hive.executeQuery(qs)
	

	def grantPermissions(self, role, permissions, database, table, column=None):
		tableString = '{}.{}'.format(database, table)
		# If permission already granted
		if tableString in self.currentPermissions:
			if permissions in self.currentPermissions[tableString]:
				if role in self.currentPermissions[tableString][permissions]:
					statement = "Correct: {} on table {}.{} for role `{}`".format(permissions, database, table, role)
					print statement
					return
		# If column level permission required
		if column:
			statement = "GRANT {}({}) ON TABLE {}.{} TO ROLE `{}`".format(permissions, column, database, table, role)
			print statement
			if self.execute:
				print "EXECUTED"
				self.hive.applyPermissions(database, table, column, role, permissions)
			else:
				with open(self.config.REVIEW_FILE, 'a+') as forReview:
					forReview.write(statement)	
			return
		# Other permissions required are table level
		statement = "GRANT {} ON TABLE {}.{} TO ROLE `{}`".format(permissions, database, table, role)
		print statement
		if self.execute:
			print "EXECUTED"
			self.hive.applyPermissions(database, table, role, permissions)
		else:
			with open(self.config.REVIEW_FILE, 'a+') as forReview:
				forReview.write(statement)	
		return
		
	

	def revokePermissions(self, allowedRoles, permissions, database, table, column=None):
		tableString = '{}.{}'.format(database, table)
		if tableString in self.currentPermissions:
			if permissions in self.currentPermissions[tableString]:
				for role in self.currentPermissions[tableString][permissions]:
					if role not in allowedRoles:
						if not column:
							statement = "REVOKE {} ON TABLE {}.{} FROM ROLE `{}`".format(permissions, database, table, role)
							print statement
							if self.execute:
								print "EXECUTED"
								self.hive.revokePermissions(database, table, role, permissions)
							else:
								with open(self.config.REVIEW_FILE, 'a+') as forReview:
									forReview.write(statement)	
							return
						statement = "REVOKE {}({}) ON TABLE {}.{} FROM ROLE `{}`".format(permissions, column, database, table, role)
						print statement
						if self.execute:
							print "EXECUTED"
							self.hive.revokePermissions(database, table, column, role, permissions)
						else:
							with open(self.config.REVIEW_FILE, 'a+') as forReview:
								forReview.write(statement)	
		return
