class Config:
	LOG_NAME 		= 'Alcatraz Policy Engine' # Base name for logging
	EXECUTE			= False # Apply permissions to systems automatically
	REVIEW_FILE 		= 'pendingPermissions.txt' # Where to log actions for review before execution
	HIVE_CONN_STR		= "jdbc:hive2://hn65.nonprod.cdh.ai.cba:10000;AuthMech=1;KrbRealm=AU.CBAINET.COM;KrbHostFQDN=hn65.nonprod.cdh.ai.cba;KrbServiceName=hive"
	HDFS_CONN_STR		= "hdfs://hn57.nonprod.cdh.ai.cba"
	HDFS_PRINCIPAL		= "hdfs/_HOST@AU.CBAINET.COM"
	CLASSPATH		= "/opt/PolicyEngine/JARs/PolicyEnforcer.jar"
	METADATA		= "./Metadata/"
