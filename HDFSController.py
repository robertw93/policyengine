import syslog
from jpype import isJVMStarted, startJVM,getDefaultJVMPath, JClass

"""
    This is the class that interacts with the HDFS file system.
    Anything that needs to access HDFS, such as permission checking or updating will go through here
"""
class HDFSClient:
    def __init__(self, config):
        """Initialise the Class, including setting auth and URI"""
        # General Config
        self.execute = config.EXECUTE
        syslog.openlog(config.LOG_NAME + "(HDFS)")

        # Load JARs
        if not isJVMStarted():
            startJVM(getDefaultJVMPath(), "-ea", "-Djava.class.path=%s" % config.CLASSPATH)
        self.hdfs = JClass("Enforcers.HDFSEnforcer")(self.config.HDFS_CONN_STR)


    def setOwnership(self, path, user, group, permissions):
        print "Recursively updating {} to be owned by {}:{} with POSIX permissions of {}".format(path, user, group, permissions)
        if self.execute:
            hdfs.setFileOwnership(path, user, group, permissions)


    def setACLUser(self, path, user, permissions):
        print "Recursively setting ACL for {}, allowing {} to have {} permissions".format(path, user, permissions)
        if self.execute:
            hdfs.setFileACLUser(path, user, permissions)


    def setACLGroup(self, path, group, permissions):
        print "Recursively setting ACL for {}, allowing {} to have {} permissions".format(path, group, permissions)
        if self.execute:
            hdfs.setFileACLGroup(path, group, permissions)

    def revokeACL(self, path, type, member):
        print "Recursively removing ACL from {}, revoking access from {}".format(path, member)
        if self.execute:
            hdfs.setFileACLUser(path, user, permissions)
