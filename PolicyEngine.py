import sys, os, json, syslog, sh, jpype
import HiveController, HDFSController, config

system_config = config.Config()                                                          
# Renew our kerberos ticket with hadmin
#sh.kinit("acoe_omnds_tintg_apt@AU.CBAINET.COM", "-k", "-t", "./pekeytab.keytab") # TODO: change keytab file
#HDFSClient = HDFSController.HDFSClient(system_config)
HiveClient = HiveController.HiveClient(system_config)
syslog.openlog(system_config.LOG_NAME)

def init():
    # Remove previous review files
    try:
        open(system_config.REVIEW_FILE, 'w').close()
    except:
        pass
    with open(system_config.METADATA + "CurrentClassifications.json") as hive_json:
        hive = json.load(hive_json)
    return (hive)


if __name__ == '__main__':
    hiveClassification = init()
    currentState = {}
    print '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Starting permission and privilege check ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
    print 'SYSTEM: Hive'
    databases = HiveClient.executeQuery("SHOW DATABASES")
    while databases.next():
	db = databases.getString("database_name")
        print db
	try:
	    tables = HiveClient.executeQuery("SHOW TABLES IN %s" % db)
	    while tables.next():
                tbl = str(tables.getString("tab_name"))
                tableString = "%s.%s" % (db, tbl)
                if tableString in hiveClassification:
                    currentClassification = hiveClassification[tableString]
                else:
                    currentClassification = { "all": [], "select": ['dft_dft_gu'] } # TODO: put default service groups here
		for permission in currentClassification:
		    HiveClient.revokePermissions(currentClassification[permission], permission, db, tbl)
                    for role in currentClassification[permission]:
                        HiveClient.grantPermissions(role, permission, db, tbl)
                currentState[tableString] = currentClassification
	except Exception as e:
	    print e
    with open(system_config.METADATA + "hiveCurrentState.json", "w+") as hive_current_state:
        hive_current_state.write(json.dumps(currentState, indent=4, sort_keys=True, separators=(',', ': '), ensure_ascii=False))
    
    
    if jpype.isJVMStarted():
        jpype.shutdownJVM()
